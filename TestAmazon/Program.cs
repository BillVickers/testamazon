﻿using System;
using System.Web;
using System.Collections.Generic;
using TestAmazon.AmazonService;
using System.Linq;
using System.ServiceModel;
using System.Configuration;
using System.Diagnostics;

namespace TestAmazon
{
    class Program
    {
        public static Cart sessionCart = new Cart();

        static void Main(string[] args)
        {
            #region CRUD tests

            try
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                List<Product> List = Search("mens shorts");

                //Product product = ItemSearch("B074QP8NZN");

                //CartAdd("134-7078907-2310551", "fJ+g0z9K7B7rsPfHM6BEgS1JHrQ=", "B00A755SKA", "1");
                //CartAdd("134-7078907-2310551", "fJ+g0z9K7B7rsPfHM6BEgS1JHrQ=", "B01N78FGCF", "1");

                //CartGet("134-7078907-2310551", "fJ+g0z9K7B7rsPfHM6BEgS1JHrQ=");
                //CartGet("131-4169298-3035318", "YjISW1sAgjOoL9mSeti+LDjfv7o=");
                //CartGet("140-8131908-6852439", "dRMZPlB6uFzh+uUnYBV2wFAImUA=");               

                //CartCreate("B01N78FGCF", "2");
                //CartClear("134-7078907-2310551", "fJ+g0z9K7B7rsPfHM6BEgS1JHrQ=");
                //CartModify("134-7078907-2310551", "fJ+g0z9K7B7rsPfHM6BEgS1JHrQ=", "C3IWYNFVSG5336", "1");

                sw.Stop();
                Console.WriteLine(sw.Elapsed);
                Console.ReadLine();
            }

            catch (Exception e)
            {
                Console.WriteLine("\nError: " + e.StackTrace);
                Console.WriteLine("\nError: " + e.Message);
                Console.WriteLine("\nInnerException: " + e.InnerException);
                Console.ReadLine();
            }

            #endregion
        }

        private static void CartCreate(string asin, string quantity)
        {
            AWSECommerceServicePortTypeClient amazonClient = CreateClient();

            CartCreateRequestItem createCartItem = new CartCreateRequestItem();
            createCartItem.ASIN = asin;
            createCartItem.Quantity = quantity;

            CartCreateRequest createRequest = new CartCreateRequest();
            createRequest.Items = new CartCreateRequestItem[] { createCartItem };

            CartCreate cart = new CartCreate();
            cart.AWSAccessKeyId = ConfigurationManager.AppSettings["accessKeyId"];
            cart.AssociateTag = ConfigurationManager.AppSettings["associateTag"];
            cart.Request = new CartCreateRequest[] { createRequest };

            CartCreateResponse response = amazonClient.CartCreate(cart);

            if (response.Cart != null)
            {
                sessionCart.CartID = response.Cart[0].CartId;
                sessionCart.PurchaseURL = response.Cart[0].PurchaseURL;
                sessionCart.HMAC = response.Cart[0].HMAC;

                if (sessionCart.PurchaseURL == null)
                {
                    Console.WriteLine("\nItem not available??? No PurchaseURL created...");
                }
                else
                {
                    foreach (var vcart in response.Cart[0].CartItems.CartItem.ToList())
                    {
                        CartItem cartItem = new CartItem();

                        cartItem.CartItemID = vcart.CartItemId;
                        cartItem.ASIN = vcart.ASIN;
                        cartItem.Quantity = vcart.Quantity;

                        sessionCart.CartItemList.Add(cartItem);
                    }
                }
            }
        }

        private static void CartGet(string cartID, string hmac)
        {
            AWSECommerceServicePortTypeClient amazonClient = CreateClient();

            CartGetRequest cartGetRequest = new CartGetRequest();
            cartGetRequest.CartId = cartID;
            cartGetRequest.HMAC = hmac;

            CartGet cart = new CartGet();
            cart.AWSAccessKeyId = ConfigurationManager.AppSettings["accessKeyId"];
            cart.AssociateTag = ConfigurationManager.AppSettings["associateTag"];
            cart.Request = new CartGetRequest[] { cartGetRequest };

            CartGetResponse response = amazonClient.CartGet(cart);

            if (response.Cart != null)
            {
                Console.WriteLine("\nCartID: " + response.Cart[0].CartId);
                //Console.WriteLine("HMAC: " + response.Cart[0].HMAC);
                if (response.Cart[0].SubTotal != null)
                {
                    Console.WriteLine("SubTotal: " + response.Cart[0].SubTotal.FormattedPrice);
                }
                //Console.WriteLine("PurchaseURL: " + response.Cart[0].PurchaseURL);

                if (response.Cart[0].CartItems != null)
                {
                    foreach (var item in response.Cart[0].CartItems.CartItem.ToList())
                    {
                        //Console.WriteLine(item.ASIN + "  " + item.Title + "  " + item.ItemTotal.FormattedPrice);
                        Console.WriteLine(item.ASIN + "  "  + item.ItemTotal.FormattedPrice);
                    }
                }
                else
                {
                    Console.WriteLine("No items in cart...");
                }
            }
        }

        private static void CartAdd(string cartID, string hmac, string asin, string quantity)
        {
            AWSECommerceServicePortTypeClient amazonClient = CreateClient();           

            CartAddRequestItem addCartItem = new CartAddRequestItem();
            addCartItem.ASIN = asin;
            addCartItem.Quantity = quantity;     

            CartAddRequest addRequest = new CartAddRequest();
            addRequest.Items = new CartAddRequestItem[] { addCartItem };
            addRequest.CartId = cartID;
            addRequest.HMAC = hmac;
                        
            CartAdd cartAdd = new CartAdd();
            cartAdd.AWSAccessKeyId = ConfigurationManager.AppSettings["accessKeyId"];
            cartAdd.AssociateTag = ConfigurationManager.AppSettings["associateTag"];
            cartAdd.Request = new CartAddRequest[] { addRequest };

            CartAddResponse response = amazonClient.CartAdd(cartAdd);

            if (response.Cart != null)
            {
                if (response.Cart[0].PurchaseURL == null)
                {
                    Console.WriteLine("\nItem not available??? No PurchaseURL created...");
                }

                //foreach (var vcart in response.Cart[0].CartItems.CartItem.ToList())
                //{
                //    var s = vcart.MetaData.ToList();

                //    var v = "";                   
                //}
            }
        }

        private static void CartModify(string cartID, string hmac, string cartItemID, string quantity)
        {
            AWSECommerceServicePortTypeClient amazonClient = CreateClient();

            CartModifyRequestItem modifyCartItem = new CartModifyRequestItem();
            modifyCartItem.CartItemId = cartItemID;
            modifyCartItem.Quantity = quantity;
            modifyCartItem.Action = CartModifyRequestItemAction.SaveForLater;
            modifyCartItem.ActionSpecified = true;

            CartModifyRequest modifyRequest = new CartModifyRequest();
            modifyRequest.CartId = cartID;
            modifyRequest.HMAC = hmac;
            modifyRequest.Items = new CartModifyRequestItem[] { modifyCartItem };

            CartModify cartModify = new CartModify();
            cartModify.AWSAccessKeyId = ConfigurationManager.AppSettings["accessKeyId"];
            cartModify.AssociateTag = ConfigurationManager.AppSettings["associateTag"];
            cartModify.Request = new CartModifyRequest[] { modifyRequest };

            CartModifyResponse response = amazonClient.CartModify(cartModify);             
        }

        private static void CartClear(string cartID, string hmac)
        {
            AWSECommerceServicePortTypeClient amazonClient = CreateClient();

            CartClearRequest cartClearRequest = new CartClearRequest();
            cartClearRequest.CartId = cartID;
            cartClearRequest.HMAC = hmac;

            CartClear cartClear = new CartClear();
            cartClear.AWSAccessKeyId = ConfigurationManager.AppSettings["accessKeyId"];
            cartClear.AssociateTag = ConfigurationManager.AppSettings["associateTag"];
            cartClear.Request = new CartClearRequest[] { cartClearRequest };

            CartClearResponse response = amazonClient.CartClear(cartClear);
        }

        private static List<Product> Search(string searchRequest)
        {
            List<Product> vList = new List<Product>();

            AWSECommerceServicePortTypeClient amazonClient = CreateClient();

            ItemSearchRequest search = new ItemSearchRequest();
            search.SearchIndex = "All";
            search.ResponseGroup = new string[] { "Large" };
            search.Keywords = HttpUtility.HtmlEncode(searchRequest);

            ItemSearch itemSearch = new ItemSearch();
            itemSearch.Request = new ItemSearchRequest[] { search };
            itemSearch.AWSAccessKeyId = ConfigurationManager.AppSettings["accessKeyId"];
            itemSearch.AssociateTag = ConfigurationManager.AppSettings["associateTag"];

            ItemSearchResponse response = amazonClient.ItemSearch(itemSearch);

            if (response.Items[0].Item != null)
            {
                foreach (var item in response.Items[0].Item.ToList())
                {
                    bool isPrime = false;

                    if (item.Offers.Offer != null)
                    {
                        foreach (var offer in item.Offers.Offer.ToList())
                        {
                            isPrime = offer.OfferListing[0].IsEligibleForPrime;
                        }
                    }

                    if (isPrime)
                    {
                        Product vmodel = new Product();

                        vmodel.Title = item.ItemAttributes.Title;
                        vmodel.ASIN = item.ASIN;                       

                        if (item.SmallImage != null)
                        {
                            vmodel.SmallImage = item.SmallImage.URL;
                        }
                        if (item.MediumImage != null)
                        {
                            vmodel.MediumImage = item.MediumImage.URL;
                        }
                        if (item.LargeImage != null)
                        {
                            vmodel.LargeImage = item.LargeImage.URL;
                        }
                        if (item.ItemAttributes.Department != null)
                        {
                            vmodel.Department = item.ItemAttributes.Department;
                        }
                        if (item.ItemAttributes.ProductGroup != null)
                        {
                            vmodel.ProductGroup = item.ItemAttributes.ProductGroup;
                        }
                        if (item.ItemAttributes.ProductTypeName != null)
                        {
                            vmodel.ProductTypeName = item.ItemAttributes.ProductTypeName;
                        }
                        if (item.ItemAttributes.Brand != null)
                        {
                            vmodel.Brand = item.ItemAttributes.Brand;
                        }
                        if (item.ItemAttributes.Size != null)
                        {
                            vmodel.Size = item.ItemAttributes.Size;
                        }
                        if (item.ItemAttributes.Color != null)
                        {
                            vmodel.Color = item.ItemAttributes.Color;
                        }
                        if (item.ItemAttributes.ListPrice != null)
                        {
                            vmodel.ListPrice = item.ItemAttributes.ListPrice.FormattedPrice;
                        }
                        if (item.OfferSummary.LowestNewPrice != null)
                        {
                            vmodel.LowestNewPrice = item.OfferSummary.LowestNewPrice.FormattedPrice;
                        }
                        if (item.EditorialReviews != null)
                        {
                            foreach (var review in item.EditorialReviews.ToList())
                            {
                                vmodel.Description = review.Content;
                            }
                        }
                        if (item.SimilarProducts != null)
                        {
                            foreach (var similar in item.SimilarProducts.ToList())
                            {
                                SimilarProduct vsimilar = new SimilarProduct();
                                vsimilar.ASIN = similar.ASIN;
                                vsimilar.Title = similar.Title;

                                vmodel.SimilarProducts.Add(vsimilar);
                            }
                        }
                        if (item.ImageSets != null)
                        {
                            foreach (var imageSet in item.ImageSets.ToList())
                            {
                                ImageSet set = new ImageSet();
                                set.Category = imageSet.Category;
                                set.SwatchImage = imageSet.SwatchImage.URL;
                                set.SmallImage = imageSet.SmallImage.URL;
                                set.ThumbnailImage = imageSet.ThumbnailImage.URL;
                                set.TinyImage = imageSet.TinyImage.URL;
                                set.MediumImage = imageSet.MediumImage.URL;
                                set.LargeImage = imageSet.LargeImage.URL;
                                if (imageSet.HiResImage != null)
                                {
                                    set.HiResImage = imageSet.HiResImage.URL;
                                }

                                vmodel.ImageSets.Add(set);
                            }
                        }
                        if (item.ItemAttributes.Feature != null)
                        {
                            foreach (var feature in item.ItemAttributes.Feature.ToList())
                            {
                                Feature vfeature = new Feature();
                                vfeature.Description = feature;

                                vmodel.Features.Add(vfeature);
                            }
                        }

                        vmodel.IsPrime = isPrime;
                        vList.Add(vmodel);
                    }
                }
            }
            else
            {
                Product vmodel = new Product()
                {
                    Title = "Please enter a correct search value"
                };
                vList.Add(vmodel);

                Console.WriteLine(vmodel.Title);
            }

            foreach (var item in vList)
            {
                Console.WriteLine(item.ASIN + " " + item.IsPrime);
            }

            return vList;
        }

        private static Product ItemSearch(string asin)
        {
            Product product = new Product();

            AWSECommerceServicePortTypeClient amazonClient = CreateClient();

            ItemLookupRequest search = new ItemLookupRequest();
            search.ResponseGroup = new string[] { "Large" };
            search.IdType = ItemLookupRequestIdType.ASIN;
            search.ItemId = new string[] { HttpUtility.HtmlEncode(asin) };

            ItemLookup itemSearch = new ItemLookup();
            itemSearch.Request = new ItemLookupRequest[] { search };
            itemSearch.AWSAccessKeyId = ConfigurationManager.AppSettings["accessKeyId"];
            itemSearch.AssociateTag = ConfigurationManager.AppSettings["associateTag"];

            ItemLookupResponse response = amazonClient.ItemLookup(itemSearch);

            return product;
        }
            private static AWSECommerceServicePortTypeClient CreateClient()
        {
            BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.Transport);
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferSize = int.MaxValue;
            AWSECommerceServicePortTypeClient amazonClient = new AWSECommerceServicePortTypeClient(
                        binding,
                        new EndpointAddress("https://webservices.amazon.com/onca/soap?Service=AWSECommerceService"));
            amazonClient.ChannelFactory.Endpoint.Behaviors.Add(new Signature());

            return amazonClient;
        }

    }
}