﻿
namespace TestAmazon
{
    public class ImageSet
    {
        public ImageSet() { }

        public string Category { get; set; } = string.Empty;
        public string SwatchImage { get; set; } = string.Empty;
        public string SmallImage { get; set; } = string.Empty;
        public string ThumbnailImage { get; set; } = string.Empty;
        public string TinyImage { get; set; } = string.Empty;
        public string MediumImage { get; set; } = string.Empty;
        public string LargeImage { get; set; } = string.Empty;
        public string HiResImage { get; set; } = string.Empty;
    }
}
