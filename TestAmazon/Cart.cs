﻿using System;
using System.Collections.Generic;

namespace TestAmazon
{
    public class Cart
    {
        public Cart() { }

        public string CartID { get; set; } = string.Empty;
        public string HMAC { get; set; } = string.Empty;
        public string PurchaseURL { get; set; } = string.Empty;      
        public List<CartItem> CartItemList { get; set; } = new List<CartItem>();
    }
}
