﻿
namespace TestAmazon
{
    public class CartItem
    {
        public CartItem() { }

        public string CartItemID { get; set; } = string.Empty;
        public string ASIN { get; set; } = string.Empty;
        public string Quantity { get; set; } = string.Empty;
    }
}
