﻿
using System.Collections.Generic;

namespace TestAmazon
{
    public class Product
    {
        public Product() { }

        public string Title { get; set; } = string.Empty;
        public string ASIN { get; set; } = string.Empty;
        public string ListPrice { get; set; } = string.Empty;
        public string LowestNewPrice { get; set; } = string.Empty;
        public string SmallImage { get; set; } = string.Empty;
        public string MediumImage { get; set; } = string.Empty;
        public string LargeImage { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string Department { get; set; } = string.Empty;
        public string ProductGroup { get; set; } = string.Empty;       
        public string ProductTypeName { get; set; } = string.Empty;
        public string Brand { get; set; } = string.Empty;
        public string Size { get; set; } = string.Empty;
        public string Color { get; set; } = string.Empty;
        public bool IsPrime { get; set; } = false;
        public List<SimilarProduct> SimilarProducts { get; set; } = new List<SimilarProduct>();
        public List<ImageSet> ImageSets { get; set; } = new List<ImageSet>();
        public List<Feature> Features { get; set; } = new List<Feature>();
    }
}
